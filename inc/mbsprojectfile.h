#ifndef MBSPROJECTFILE_H
#define MBSPROJECTFILE_H

#include <QObject>
#include "mbsconnectionsettings.h"

class QDomDocument;
class MbsV0_9ProjectFile;
class QIODevice;

class MbsProjectFile : public QObject
{
    Q_OBJECT
public:
    explicit MbsProjectFile( QObject *parent = 0 );

    bool LoadFile(QIODevice &io, QString *err_msg, int *err_line, int *err_col);
    virtual bool SaveFile( QString path ) = 0;

    const MbsConnectionSettings &GetConnectionSettings() const { return m_connection_settings; }


    friend class MbsV0_9ProjectFile;
signals:

public slots:

protected:
    virtual bool ParseConnectionSettings(const QDomDocument &document) = 0;

    static QDomElement FindElement( const QDomElement &node, QString node_name );
protected:
    MbsConnectionSettings m_connection_settings;
};

#endif // MBSPROJECTFILE_H

#ifndef MbsV0_9Parser_H
#define MbsV0_9Parser_H

#include <QObject>
#include <QMessageBox>
#include <QDomDocument>
#include "mbsprojectfile.h"

class MbsV0_9ProjectFile : public MbsProjectFile
{
    Q_OBJECT
public:    
    explicit MbsV0_9ProjectFile();

    bool SaveFile( QString path );

protected:
    bool ParseConnectionSettings(const QDomDocument &document);

signals:

public slots:

private:
};

#endif // MbsV0_9Parser_H

#ifndef KEYSVALUESOBJECT_H
#define KEYSVALUESOBJECT_H

#include <QObject>

template<class T, class U>
class QMap;

class QDomDocument;
class QDomElement;

class ParsedXmlNode : public QObject
{
    Q_OBJECT
public:
    explicit ParsedXmlNode(QObject *parent = 0);

    /*!
     * \brief ChangeKeyTagMapping
     * Change the key mapping between a key and the tag in xml. This function
     * will change the used key, so if in the settings map the key was already used, the value is set
     * to the new key_value
     *
     * \param key The key you want to change
     * \param key_value The new key value
     */
    void ChangeKeyTagMapping( u_int32_t key, const QString key_value );

    /*!
     * \brief ChangeKeyAttributeMapping
     * Change the key mapping between a key and the attribute name in xml. This function
     * will change the used key, so if in the settings map the key was already used, the value is set
     * to the new key_value
     *
     * \param key The key you want to change
     * \param key_value The new key value
     */
    void ChangeKeyAttributeMapping( u_int32_t key, const QString attribute_value );

    bool GetText( u_int32_t key, QString &value );
    bool GetAttribute( u_int32_t key, QString &value );

    bool StoreInDomElement( const QDomDocument &document, QDomElement &element ) const;
    void ParseDomElement( const QDomElement &element );
signals:

public slots:

protected:
    QMap<QString, QString> *m_texts;
    QMap<u_int32_t, QString> *m_key_tag_mapping;

    QMap<QString, QString> *m_attributes;
    QMap<u_int32_t, QString> *m_key_attribute_mapping;

    /* also add attributes? */
};

#endif // KEYSVALUESOBJECT_H

#ifndef MBSCONNECTIONSETTINGS_H
#define MBSCONNECTIONSETTINGS_H

#include <QObject>
#include "keysvaluesobject.h"

class MbsProjectFile;

class MbsConnectionSettings : public ParsedXmlNode
{
public:
    enum Key
    {
        Ip=0,
        Port,
        PollTime,
        SlaveId,
        Timeout,
        ConsecutiveMax
    };

    /*!
     * \brief MbsConnectionSettings
     * Create a new object for the connection settings
     *
     * \param parent
     */
    explicit MbsConnectionSettings( QObject *parent = 0 );

    bool GetIp( QString &value );
    bool GetPort( QString &value );
    bool GetPollTime( QString &value );
    bool GetSlaveId( QString &value );
    bool GetTimeout( QString &value );
    bool GetConsecutiveMax( QString &value );
signals:

public slots:

};

#endif // MBSCONNECTIONSETTINGS_H

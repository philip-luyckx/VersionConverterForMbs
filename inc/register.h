#ifndef REGISTER_H
#define REGISTER_H

#include <QObject>
#include "keysvaluesobject.h"

class QDomElement;

class Register : public ParsedXmlNode
{
public:
    enum Key
    {
        Address = 0,
        Text,
        Unsigned,
        Multiply,
        Divide,
        Color
    };

    explicit Register(QObject *parent = 0);

    void ParseElement( QDomElement &element );

signals:

public slots:

private:
};

#endif // REGISTER_H

#-------------------------------------------------
#
# Project created by QtCreator 2015-08-13T22:18:02
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MbsMbcCreator
TEMPLATE = app


SOURCES += src/main.cpp\
        src/mainwindow.cpp \
    src/MbsV0_9ProjectFile.cpp \
    src/mbsprojectfile.cpp \
    src/mbsconnectionsettings.cpp \
    src/register.cpp \
    src/keysvaluesobject.cpp

HEADERS  += inc/mainwindow.h \
    inc/MbsV0_9ProjectFile.h \
    inc/mbsprojectfile.h \
    inc/mbsconnectionsettings.h \
    inc/register.h \
    inc/keysvaluesobject.h

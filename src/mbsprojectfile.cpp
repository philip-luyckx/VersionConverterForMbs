
#include <QDomElement>
#include <QIODevice>

#include "inc/mbsprojectfile.h"

MbsProjectFile::MbsProjectFile(QObject *parent) : QObject(parent)
{

}


bool MbsProjectFile::LoadFile(QIODevice &io, QString *err_msg, int *err_line, int *err_col)
{
    QDomDocument document;
    bool success = document.setContent(&io, err_msg, err_line, err_col);

    if( success )
    {
        success = ParseConnectionSettings( document );
    }

    return success;
}

QDomElement MbsProjectFile::FindElement( const QDomElement &node, QString node_name )
{
    QDomElement child = node.firstChildElement();

    while( !child.isNull() && child.nodeName() != node_name )
    {
        child = child.nextSiblingElement();
    }

    return child;
}


#include <QDomElement>
#include <QDomDocument>

#include "inc/register.h"

Register::Register(QObject *parent) :
    ParsedXmlNode(parent)
{
}

void Register::ParseElement( QDomElement &element )
{
    ParsedXmlNode::ParseDomElement( element );


}


#include <QDebug>
#include <QMap>
#include <QFile>

#include "../inc/MbsV0_9ProjectFile.h"

MbsV0_9ProjectFile::MbsV0_9ProjectFile()
{
    m_connection_settings.ChangeKeyTagMapping( MbsConnectionSettings::Ip, "ip" );
    m_connection_settings.ChangeKeyTagMapping( MbsConnectionSettings::Port, "port" );
    m_connection_settings.ChangeKeyTagMapping( MbsConnectionSettings::SlaveId, "slaveid" );
    m_connection_settings.ChangeKeyTagMapping( MbsConnectionSettings::Timeout, "timeout" );
    m_connection_settings.ChangeKeyTagMapping( MbsConnectionSettings::PollTime, "polltime" );
    m_connection_settings.ChangeKeyTagMapping( MbsConnectionSettings::ConsecutiveMax, "consecutivemax" );
}

bool MbsV0_9ProjectFile::SaveFile( QString path )
{
    QDomDocument doc;
    QDomElement root;

    root = doc.createElement("modbusscope");
    doc.appendChild(root);

    QDomElement modbus = doc.createElement("modbus");
    root.appendChild(modbus);

    QDomElement connection = doc.createElement("connection");
    GetConnectionSettings().StoreInDomElement( doc, connection );
    if( connection.childNodes().length() > 0 )
    {
        modbus.appendChild( connection );
    }

    QFile file( path );
    file.open( QFile::WriteOnly );
    QTextStream stream( &file );
    doc.save(stream, 2);

    return true;
}

bool MbsV0_9ProjectFile::ParseConnectionSettings(const QDomDocument &document)
{
    bool ret = true;

    QDomElement node = document.firstChildElement();

    qDebug() << node.nodeName();

    if( node.nodeName() != "modbusscope" )
    {
        ret = false;
    }

    if( ret )
    {
        node = FindElement( node, "modbus" );

        if( node.isNull() )
        {
            ret = false;
        }
    }

    if( ret )
    {
        node = FindElement( node, "connection" );

        if( node.isNull() )
        {
            ret = false;
        }
    }

    if( ret )
    {
        m_connection_settings.ParseDomElement( node );
    }

    return ret;
}


#include <QDebug>
#include <QFile>
#include "../inc/mainwindow.h"
#include"../inc/MbsV0_9ProjectFile.h"

int main(int argc, char *argv[])
{
    MbsV0_9ProjectFile mbs;
    QString err;
    int err_line, err_col;
    QFile file( "/home/pluyckx/dev/tmp/v0_9.mbs" );

    if( file.open(QFile::ReadOnly) )
    {
        if( !mbs.LoadFile(file, &err, &err_line, &err_col) )
        {
            qDebug() << "Error loading file:";
            qDebug().nospace() << err_line << ":" << err_col << " " << err;
        }
        else
        {
            mbs.SaveFile( "/home/pluyckx/dev/tmp/v0_9_copy.mbs" );
        }

        file.close();
    }
    else
    {
        qDebug() << "Could not open" << file.fileName();
    }
}


#include <QMap>

#include "inc/mbsconnectionsettings.h"

MbsConnectionSettings::MbsConnectionSettings( QObject *parent ) :
    ParsedXmlNode( parent )
{
}

bool MbsConnectionSettings::GetIp(QString &value)
{
    return GetText( MbsConnectionSettings::Ip, value );
}

bool MbsConnectionSettings::GetPort(QString &value)
{
    return GetText( MbsConnectionSettings::Port, value );
}

bool MbsConnectionSettings::GetPollTime(QString &value)
{
    return GetText( MbsConnectionSettings::PollTime, value );
}

bool MbsConnectionSettings::GetSlaveId(QString &value)
{
    return GetText( MbsConnectionSettings::SlaveId, value );
}

bool MbsConnectionSettings::GetTimeout(QString &value)
{
    return GetText( MbsConnectionSettings::Timeout, value );
}

bool MbsConnectionSettings::GetConsecutiveMax(QString &value)
{
    return GetText( MbsConnectionSettings::ConsecutiveMax, value );
}

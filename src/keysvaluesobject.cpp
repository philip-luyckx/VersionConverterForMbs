
#include <QMap>
#include <QDomElement>
#include <QDomDocument>

#include "inc/keysvaluesobject.h"

ParsedXmlNode::ParsedXmlNode(QObject *parent) :
    QObject(parent),
    m_texts(new QMap<QString, QString>()), m_key_tag_mapping(new QMap<u_int32_t, QString>()),
    m_attributes(new QMap<QString, QString>()), m_key_attribute_mapping(new QMap<u_int32_t, QString>())
{
}

void ParsedXmlNode::ChangeKeyTagMapping( u_int32_t key, const QString key_value )
{
    QString old_key = "";
    if( m_key_tag_mapping->contains( key ) )
    {
        old_key = (*m_key_tag_mapping)[key];
    }

    (*m_key_tag_mapping)[key] = key_value;

    if( m_texts->contains(old_key) )
    {
        QString value = (*m_texts)[old_key];
        m_texts->remove( old_key );
        (*m_texts)[key_value] = value;
    }
}

void ParsedXmlNode::ChangeKeyAttributeMapping( u_int32_t key, const QString key_value )
{
    QString old_key = "";
    if( m_key_attribute_mapping->contains( key ) )
    {
        old_key = (*m_key_attribute_mapping)[key];
    }

    (*m_key_attribute_mapping)[key] = key_value;

    if( m_attributes->contains(old_key) )
    {
        QString value = (*m_attributes)[old_key];
        m_attributes->remove( old_key );
        (*m_attributes)[key_value] = value;
    }
}

bool ParsedXmlNode::GetText( u_int32_t key, QString &value )
{

    if( m_key_tag_mapping->contains( key ) )
    {
        if( m_texts->contains((*m_key_tag_mapping)[key]) )
        {
            value = (*m_texts)[(*m_key_tag_mapping)[key]];
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

bool ParsedXmlNode::GetAttribute( u_int32_t key, QString &value )
{

    if( m_key_attribute_mapping->contains( key ) )
    {
        if( m_attributes->contains((*m_key_attribute_mapping)[key]) )
        {
            value = (*m_attributes)[(*m_key_attribute_mapping)[key]];
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

bool ParsedXmlNode::StoreInDomElement( const QDomDocument &document, QDomElement &element ) const
{
    QDomElement child;

    QList<QString> keys = m_texts->keys();

    for( int i=0; i<keys.length(); i++ )
    {
        child = document.createElement(keys[i]);
        child.appendChild( document.createTextNode( (*m_texts)[keys[i]] ) );

        element.appendChild( child );
    }

    keys = m_attributes->keys();

    for( int i=0; i<keys.length(); i++ )
    {
        QDomAttr attr = document.createAttribute( keys[i] );
        attr.setValue( (*m_attributes)[keys[i]]);
        element.appendChild( attr );
    }

    return true;
}

void ParsedXmlNode::ParseDomElement( const QDomElement &element )
{
    QDomElement child = element.firstChildElement();

    while( !child.isNull() )
    {
        /* Only store elements which has no child elements, children are removed by default */
        if( !child.hasChildNodes() )
        {
            (*m_texts)[child.nodeName()] = child.text();
            child = child.nextSiblingElement();
        }
    }

    QDomNamedNodeMap attributes = element.attributes();

    for( uint32_t i=0; i<attributes.length(); i++ )
    {
        QDomNode node = attributes.item( i );

        if( node.isAttr() )
        {
            QDomAttr attr = node.toAttr();

            (*m_attributes)[attr.name()] = attr.value();
        }
    }
}
